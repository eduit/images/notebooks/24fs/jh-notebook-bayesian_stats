FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-16

USER root

COPY start-singleuser.sh /usr/local/bin/

RUN mamba env create -f /builds/eduit/images/notebooks/24fs/jh-notebook-bayesian_stats/environment.yaml && \
  source activate stats_course && \
  python -m ipykernel install --name=stats_course --display-name='Bayesian Statistics'

USER jovyan
